module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      from: '0xEf0F837efB9e9444Da61D0125abA9B005F18948e',
      network_id: "*"
    }
  }
};
