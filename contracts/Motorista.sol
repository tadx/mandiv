pragma solidity ^0.5.0;

import './Viagem.sol';
import './Itinerario.sol';
import './Maplist.sol';

contract Motorista{

	struct Veiculo{
		string documentacao;
		string modelo;
		uint vagas;
	}

	Veiculo[] veiculos;
	address public addr;
	string licenca;
	bool status;
	Maplist public viagens;
	Maplist public itinerarios;

	constructor() public{
		status = true;
		addr = msg.sender;
		viagens = new Maplist();
		itinerarios = new Maplist();
	}

	modifier isOwner(){
        require(msg.sender == addr);
        _;
    }
	
	function registraVeiculo(string memory _doc, string memory _mod, uint _vagas) public{
		Veiculo memory v = Veiculo(_doc,_mod,_vagas);
		veiculos.push(v);
	}

	function setStatus(bool _stat) public isOwner{
		status = _stat;
	}

	function ativo() public view returns(bool){
		return status;
	}

	function myAddr() public view returns(address){
		return msg.sender;
	}

	function criaViagem() public{
		Viagem v = new Viagem();
		viagens.insere(address(v));
	}

	function setaItinerario(address _i, address _v) public{
		Viagem v = Viagem(_v);
		v.atualizaItinerario(_i);
	}

	function criaItinerario() public{
		Itinerario i = new Itinerario();
		itinerarios.insere(address(i));
	}

	function acessaItinerario(uint _n) public view returns (address){
		address primeiro = address(itinerarios.primeiro());
		if(_n == 0){
			return primeiro;
		}
		else{
			if(_n>itinerarios.tamanho()){
				return 0x0000000000000000000000000000000000000000;
			}
			address atual = primeiro;
			uint i;
			for (i == 0; i < _n; i++){
				atual = itinerarios.mapa(atual);
			}
			return atual;
		}
	}

	function acessaViagem(uint _n) public view returns (address){
		address primeiro = address(viagens.primeiro());
		if(_n == 0){
			return primeiro;
		}
		else{
			if(_n>viagens.tamanho()){
				return 0x0000000000000000000000000000000000000000;
			}
			address atual = primeiro;
			uint i;
			for (i == 0; i < _n; i++){
				atual = viagens.mapa(atual);
			}
			return atual;
		}
	}

	function incluiPonto(address _n, string memory _rua, string memory _x, string memory _y) public{
		Itinerario it = Itinerario(_n);
		it.acrescentaPonto(_rua,_x,_y);
	}

	//function atualizaLicenca(string _oracchainId)
	//function avaliaPassageiro(string _viagem, string _passageiro)
}