pragma solidity ^0.5.0;

import './Viagem.sol';
import './Maplist.sol';

contract Usuario{
    
    struct Endereco{
        string pais;
        string estado;
        string cidade;
        string bairro;
        string rua;
        uint num;
        uint complemento;
    }

	string nome;
    uint avaliacao;
    string endereco;
    address addr;
    Maplist public viagens;

    constructor(string memory _n, address _a) public{
    	nome = _n;
    	avaliacao = 100;
    	addr = _a;
        viagens = new Maplist();
    }

    modifier isOwner(){
        require(msg.sender == addr);
        _;
    }

    function myAddr() public view returns(address){
    	return addr;
    }

    function name() public view returns(string memory _nome){
        return nome;
    }

    function atualizaEndereco(string memory _newEnd) public isOwner{
        endereco = _newEnd;
    }

    function novaAvaliacao(uint _av) public{
        avaliacao = (9*avaliacao + _av)/10;
    }

    function getAvaliacao() public view returns(uint){
        return avaliacao;
    }

    function reservarViagem(address _vEnd) public{
        Viagem v = Viagem(_vEnd);
        v.incluiPassageiro(msg.sender);
        viagens.insere(msg.sender);
    }

    function avaliaViagem(address _vEnd, uint _av) public{
        Viagem v = Viagem(_vEnd);
        v.avalia(_av);
    }
}