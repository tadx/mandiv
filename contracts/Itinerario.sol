pragma solidity ^0.5.0;

contract Itinerario{
    
    struct Ponto{
        string x;
        string y;
    }

    struct Segmento{
        string xa;
        string xb;
        string ya;
        string yb;
        //Ponto b;
        string rua;
    }

    string inix;
    string iniy;
    string fimx;
    string fimy;
    mapping(uint=>Segmento) public waypoints;
    uint n_pontos = 0;
    uint dist;

    constructor() public{
        inix = 'A';
        fimx = inix;
        iniy = 'B';
        fimy = iniy;
        n_pontos = 1;
        dist = 0;
    }

    function acrescentaPonto(string memory _rua, string memory _x, string memory _y) public{
        //Ponto memory pf = Ponto(_fimy,_fimy);
        //Ponto memory pi = waypoints[n_pontos-1].a;
        waypoints[n_pontos] = Segmento(fimx,_x,fimy,_y,_rua);
        fimx = _x;
        fimy = _y;
        dist += 10; //valor ficticio
    }

    function getStreet(uint _i) public view returns (string memory seg){
        return waypoints[_i].rua;
    }

    function getDist() public view returns (uint distancia){
        return dist;
    }

    //function visualiza() public view returns ()
}