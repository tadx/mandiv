pragma solidity ^0.5.0;

import './Itinerario.sol';
import './Maplist.sol';

contract Viagem{
    
	Itinerario i;
	bool hasIt;
	uint vagas;
	address motorista;
	uint dist;
	uint ppp; //preco por pessoa
	uint min; //lotacao minima
	uint max;
	uint combustivel;
	uint consumo;
	uint precoBase;
	uint flutuacao;
	uint data;
	uint avaliacao;
	Maplist passageiros;
	uint nPassageiros;
	bool aberta;
	mapping(address=>uint) avaliacoes;

	modifier temItinerario(){
		require(hasIt);
		_;
	}

	modifier temVaga(){
		require(nPassageiros>max);
		_;
	}

	modifier qtdeMinima(){
		require(nPassageiros >= min);
		_;
	}

	constructor() public{
		data = 1548201600;
		vagas = 10;
		avaliacao = 1000;
		aberta = true;
		min = 1;
		max = 10;
		motorista = msg.sender;
		consumo = 10;
		dist = 0;
		nPassageiros = 0;
		passageiros = new Maplist();
		combustivel = 368258;
	}

	function status() public view returns(bool){
		return aberta;
	}

	function atualizaItinerario(address _a) public{
		i = Itinerario(_a);
		dist = i.getDist();
		precoBase = (consumo*combustivel*dist)/100;
		ppp=precoBase*2;
		hasIt = true;
	}

	function atualizaPreco(bool _entrou) internal{
		if(_entrou){
			if(nPassageiros == 0){
				flutuacao = precoBase;
				ppp = precoBase + flutuacao;
			}
			else{
				flutuacao = flutuacao*3/4;
				ppp = precoBase + flutuacao;
			}
			nPassageiros += 1;
		}
		else{
			flutuacao = flutuacao*4/3;
			ppp = precoBase + flutuacao;
			nPassageiros -= 1;
		}
	}

	function incluiPassageiro(address _id) public{
		passageiros.insere(_id);
		atualizaPreco(true);
	}

	/*function retiraPassageiro(address _id) public{
		//passageiros.remove(_id);
		atualizaPreco(false);
	}*/
	
	function avalia(uint _nota) public{
		avaliacoes[msg.sender] = _nota;
		avaliacao = (_nota + 9*avaliacao)/10;
	}

	function precoPorPessoa() public view returns(uint){
		return ppp;
	}

	function getPrecoBase() public view returns(uint){
		return precoBase;
	}

	function getDist() public view returns(uint){
		return dist;
	}

	function getFlutuacao() public view returns(uint){
		return flutuacao;
	}

	function getAvaliacao() public view returns(uint){
		return avaliacao;
	}

}