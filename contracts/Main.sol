pragma solidity >0.4.99 <0.6.0;

import './Usuario.sol';
import './Motorista.sol';

contract Main{
	
    mapping(address=>Usuario) public users;
	mapping(address=>Motorista) public drivers;
	Usuario u;

    function registra(string memory _nome) public {
        require(address(users[msg.sender])==0x0000000000000000000000000000000000000000);
    	users[msg.sender] =  new Usuario(_nome,msg.sender);
    }

    function registraMotorista() public{    
        require(address(drivers[msg.sender])==0x0000000000000000000000000000000000000000);
        drivers[msg.sender] = new Motorista();
    }

    function getUser() public view returns (Usuario){
        return u;
    }

    function setUser() public{
    	u = Usuario(users[msg.sender]);
    }
}