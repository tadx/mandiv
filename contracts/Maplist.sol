pragma solidity ^0.5.0;

contract Maplist{

	address public first;
	address public last;
	uint size;
	mapping(address=>address) public mapa;

	constructor() public{
		mapa[msg.sender] = msg.sender;
		first = msg.sender;
		last = 0x0000000000000000000000000000000000000000;
		size = 0;
	}

	function insere(address _end) public{
		if(size==0){
			first = _end;
		}
		mapa[last] = _end;
		mapa[_end] = 0x0000000000000000000000000000000000000000;
		last = _end;
		size++;
	}

	/*function remove(address _end) public{
		if(_end == first){
			first = mapa[first];
		}
		else{
			address anterior = first;
			for(uint i = 0; i < size; i++){
				if(mapa[anterior] != 0x0000000000000000000000000000000000000000 && mapa[anterior] != _end){
					anterior = mapa[anterior];
				}
			}
			require(mapa[anterior]!=0x0000000000000000000000000000000000000000);
			mapa[anterior] = mapa[_end];
			if(mapa[_end]==0x0000000000000000000000000000000000000000){
				last = anterior;
			}
		}
		size--;
		if(size==0){
			first = 0x0000000000000000000000000000000000000000;
			last = 0x0000000000000000000000000000000000000000;
		}
	}*/

	/*function imprime() public view returns (address[] memory _ends){
		address[20] memory resp;
		if(size==0) return resp;
		else{
			uint i;
			address atual = first;
			for(i=0;i<size;i++){
				resp[i] = atual;
				atual = mapa[atual];
			}
			return resp;
		}
	}*/

	function primeiro() public view returns(address){
		return first;
	}

	function tamanho() public view returns(uint){
		return size;
	}
}