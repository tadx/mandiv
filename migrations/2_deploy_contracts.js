var Main = artifacts.require("Main");
var Usuario = artifacts.require("Usuario");
var Motorista = artifacts.require("Motorista");
var Itinerario = artifacts.require("Itinerario");
var Viagem = artifacts.require("Viagem");
var Maplist = artifacts.require("Maplist");

module.exports = function(deployer) {
  deployer.deploy(Main);
  deployer.deploy(Itinerario);
  deployer.deploy(Viagem);
  deployer.deploy(Motorista);
  deployer.deploy(Usuario,'Tadeu','0xEf0F837efB9e9444Da61D0125abA9B005F18948e');
  deployer.deploy(Usuario,'Kowada','0xcC2db96F937e1cE72565781AF3aA140CC2D72D88');
};